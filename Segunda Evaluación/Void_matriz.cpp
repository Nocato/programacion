#include <stdio.h>
2
3 #define TALLA 5
4
//Void con matrizes

5 int main(void)
6 {
7 float a[TALLA][TALLA], b[TALLA][TALLA];
8 float s[TALLA][TALLA], p[TALLA][TALLA];
9 int i, j, k;
10
11 /* MATRIZ A*/
12 for (i=0; i<TALLA; i++)
13 for (j=0; j<TALLA; j++) {
14 printf ("Elemento (%d, %d): ", i, j); scanf ("%f", &a[i][j]);
15 }
16
17 /* MATRIZ B */
18 for (i=0; i<TALLA; i++)
19 for (j=0; j<TALLA; j++) {
20 printf ("Elemento (%d, %d): ", i, j); scanf ("%f", &b[i][j]);
21 }
22
23 /* CALCULO SUMA */
24 for (i=0; i<TALLA; i++)
25 for (j=0; j<TALLA; j++)
26 s[i][j] = a[i][j] + b[i][j];
27
28 /* CALCULO PRODUCTO */
for (i=0; i<TALLA; i++)
30 for (j=0; j<TALLA; j++) {
31 p[i][j] = 0.0;
32 for (k=0; k<TALLA; k++)
33 p[i][j] += a[i][k] * b[k][j];
34 }
35
36 /* RESULTADO SUMA */
37 printf ("Suma\n");
38 for (i=0; i<TALLA; i++) {
39 for (j=0; j<TALLA; j++)
40 printf ("%8.3f", s[i][j]);
41 printf ("\n");
42 }
43
44 /* RESULTADO PRODUCTO */
45 printf ("Producto\n");
46 for (i=0; i<TALLA; i++) {
47 for (j=0; j<TALLA; j++)
48 printf ("%8.3f", p[i][j]);
49 printf ("\n");
50 }
51
52 return 0;
53 }

